//
// Created by magnus on 12.03.23.
//

#include "gtest/gtest.h"



int main(int argc, char **argv) {
    // This is for usage on the command line.
    // Usage: ./Executable path "TestSuite.NameTest"
    ::testing::InitGoogleTest(&argc, argv);
    if (argc == 2) {
        ::testing::GTEST_FLAG(filter) = std::string{argv[1]};
    }
    return RUN_ALL_TESTS();
}

