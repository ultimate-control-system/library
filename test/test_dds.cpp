//
// Created by magnus on 12.03.23.
//


#include "gtest/gtest.h"

#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
//#include "rrai_control_msgs/msg/ForkliftMastControl.h"
#include "rrai_control_msgs/msg/ForkliftMastControlPubSubTypes.h"


namespace {

class PublisherF: public testing::Test{
public:
    PublisherF() {
        DomainParticipantQos participantQos;
        participantQos.name("Joystick test controller");
        Participant = DomainParticipantFactory::get_instance()->create_participant(70, participantQos);
    }
    ~PublisherF() {
        delete Participant;
    }
protected:

    DomainParticipant *Participant;

};
TEST(test_publisher, domain_creation) {
        DomainParticipant *Participant;
        DomainParticipantQos participantQos;
        participantQos.name("DDS Test");
        Participant = DomainParticipantFactory::get_instance()->create_participant(70, participantQos);

        EXPECT_NE(Participant, nullptr);
        EXPECT_TRUE(Participant->is_enabled());

    }
TEST_F(PublisherF, create_subscriber) {
    DdsSubscriber<rrai_control_msgs::msg::ForkliftMastControlPubSubType> *Subscriber;

    Subscriber = new DdsSubscriber<rrai_control_msgs::msg::ForkliftMastControlPubSubType>(Participant,"rt/demo_hive_location/cep/forklift");
    EXPECT_NE(Subscriber, nullptr);
    }



TEST_F(PublisherF, create_publisher) {
    DdsPublisher<rrai_control_msgs::msg::ForkliftMastControlPubSubType> *Publisher;

    Publisher = new DdsPublisher<rrai_control_msgs::msg::ForkliftMastControlPubSubType>(Participant,"rt/demo_hive_location/cep/forklift");
    EXPECT_NE(Publisher, nullptr);
}

}