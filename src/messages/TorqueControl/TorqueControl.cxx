// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file TorqueControl.cpp
 * This source file contains the definition of the described types in the IDL file.
 *
 * This file was generated by the tool gen.
 */

#ifdef _WIN32
// Remove linker warning LNK4221 on Visual Studio
namespace {
char dummy;
}  // namespace
#endif  // _WIN32

#include "TorqueControl.h"
#include <fastcdr/Cdr.h>

#include <fastcdr/exceptions/BadParamException.h>
using namespace eprosima::fastcdr::exception;

#include <utility>

TorqueControl::TorqueControl()
{
    // m_torque_nm com.eprosima.idl.parser.typecode.PrimitiveTypeCode@7cd62f43
    m_torque_nm = 0.0;
    // m_max_speed_rpm com.eprosima.idl.parser.typecode.PrimitiveTypeCode@6093dd95
    m_max_speed_rpm = 0.0;

}

TorqueControl::~TorqueControl()
{


}

TorqueControl::TorqueControl(
        const TorqueControl& x)
{
    m_torque_nm = x.m_torque_nm;
    m_max_speed_rpm = x.m_max_speed_rpm;
}

TorqueControl::TorqueControl(
        TorqueControl&& x)
{
    m_torque_nm = x.m_torque_nm;
    m_max_speed_rpm = x.m_max_speed_rpm;
}

TorqueControl& TorqueControl::operator =(
        const TorqueControl& x)
{

    m_torque_nm = x.m_torque_nm;
    m_max_speed_rpm = x.m_max_speed_rpm;

    return *this;
}

TorqueControl& TorqueControl::operator =(
        TorqueControl&& x)
{

    m_torque_nm = x.m_torque_nm;
    m_max_speed_rpm = x.m_max_speed_rpm;

    return *this;
}

size_t TorqueControl::getMaxCdrSerializedSize(
        size_t current_alignment)
{
    size_t initial_alignment = current_alignment;


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);



    return current_alignment - initial_alignment;
}

size_t TorqueControl::getCdrSerializedSize(
        const TorqueControl& data,
        size_t current_alignment)
{
    (void)data;
    size_t initial_alignment = current_alignment;


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);


    current_alignment += 8 + eprosima::fastcdr::Cdr::alignment(current_alignment, 8);



    return current_alignment - initial_alignment;
}

void TorqueControl::serialize(
        eprosima::fastcdr::Cdr& scdr) const
{

    scdr << m_torque_nm;
    scdr << m_max_speed_rpm;

}

void TorqueControl::deserialize(
        eprosima::fastcdr::Cdr& dcdr)
{

    dcdr >> m_torque_nm;
    dcdr >> m_max_speed_rpm;
}

/*!
 * @brief This function sets a value in member torque_nm
 * @param _torque_nm New value for member torque_nm
 */
void TorqueControl::torque_nm(
        double _torque_nm)
{
    m_torque_nm = _torque_nm;
}

/*!
 * @brief This function returns the value of member torque_nm
 * @return Value of member torque_nm
 */
double TorqueControl::torque_nm() const
{
    return m_torque_nm;
}

/*!
 * @brief This function returns a reference to member torque_nm
 * @return Reference to member torque_nm
 */
double& TorqueControl::torque_nm()
{
    return m_torque_nm;
}

/*!
 * @brief This function sets a value in member max_speed_rpm
 * @param _max_speed_rpm New value for member max_speed_rpm
 */
void TorqueControl::max_speed_rpm(
        double _max_speed_rpm)
{
    m_max_speed_rpm = _max_speed_rpm;
}

/*!
 * @brief This function returns the value of member max_speed_rpm
 * @return Value of member max_speed_rpm
 */
double TorqueControl::max_speed_rpm() const
{
    return m_max_speed_rpm;
}

/*!
 * @brief This function returns a reference to member max_speed_rpm
 * @return Reference to member max_speed_rpm
 */
double& TorqueControl::max_speed_rpm()
{
    return m_max_speed_rpm;
}


size_t TorqueControl::getKeyMaxCdrSerializedSize(
        size_t current_alignment)
{
    size_t current_align = current_alignment;





    return current_align;
}

bool TorqueControl::isKeyDefined()
{
    return false;
}

void TorqueControl::serializeKey(
        eprosima::fastcdr::Cdr& scdr) const
{
    (void) scdr;
      
}
