//
// Created by magnus on 16.09.22.
//

#ifndef DDSPUBLISHER_H
#define DDSPUBLISHER_H

#include <fastdds/dds/publisher/Publisher.hpp>
#include <fastdds/dds/publisher/DataWriter.hpp>
#include <fastdds/dds/publisher/DataWriterListener.hpp>

#include "DdsUtilities.h"

template<class T>
class DdsPublisher : public  DataWriterListener {
public:
    DdsPublisher()
        : _participant(nullptr)
        , _publisher(nullptr)
        , _topic(nullptr)
        , _writer(nullptr)
        , _type(new T)
        , _subscriberCount(0)
    {}

    template <class S>
    DdsPublisher(DomainParticipant *participant, S&& topicName)
            : _participant(nullptr)
            , _publisher(nullptr)
            , _topic(nullptr)
            , _writer(nullptr)
            , _type(new T)
            , _subscriberCount(0)
    {
        init(participant, std::forward<S>(topicName));
    }

    ~DdsPublisher() override {
        _destroy();
    }

    /*!
     *
     * @tparam S String type
     * @param participant
     * @param topicName
     * @return
     */
    template <class S>
    bool init(DomainParticipant *participant, S&& topicName)
    {
        // Destroy if it already exists
        _destroy();

        _participant = participant;

        _type.register_type(_participant);

        _topic = _participant->create_topic(std::forward<S>(topicName), _type->getName(), TOPIC_QOS_DEFAULT);
        if (!_topic) {
            return false;
        }

        _publisher = _participant->create_publisher(PUBLISHER_QOS_DEFAULT, nullptr);
        if (!_publisher) {
            return false;
        }
        // Create a DataWriter

        eprosima::fastdds::dds::DataWriterQos writer_qos = eprosima::fastdds::dds::DATAWRITER_QOS_DEFAULT;
        writer_qos.history().kind = eprosima::fastdds::dds::KEEP_LAST_HISTORY_QOS;
        writer_qos.history().depth = 1;
      writer_qos.reliability().kind = eprosima::fastdds::dds::RELIABLE_RELIABILITY_QOS;
        writer_qos.durability().kind = eprosima::fastdds::dds::VOLATILE_DURABILITY_QOS;

      writer_qos.lifespan().duration = eprosima::fastrtps::Duration_t(0, 100000000); // 100ms
      writer_qos.liveliness().kind = eprosima::fastdds::dds::AUTOMATIC_LIVELINESS_QOS;

      writer_qos.liveliness().lease_duration = eprosima::fastrtps::Duration_t(100 * 1e-3);
      writer_qos.liveliness().announcement_period = eprosima::fastrtps::Duration_t(100 * 1e-3 * 0.5);
       // writer_qos.liveliness().lease_duration = eprosima::fastrtps::Duration_t(0, 100000000); // 100ms


        _writer = _publisher->create_datawriter(_topic, writer_qos, this);
        if (!_writer) {
            return false;
        }

        return true;
    }

    bool publish(typename T::type msg)
    {
        if ((_subscriberCount > 0) && _writer)
        {
            _writer->write(&msg);
            return true;
        }
        return false;
    };

    [[nodiscard]] int32_t subscriberCount() const { return _subscriberCount; }

private:
    DomainParticipant *_participant;
    Publisher *_publisher;
    Topic *_topic;
    DataWriter *_writer;
    TypeSupport _type;
    int32_t _subscriberCount;

    void _destroy()
    {
        if (_publisher && _writer)
        {
            _publisher->delete_datawriter(_writer);
            _writer = nullptr;
        }

        if (_participant)
        {
            if (_publisher)
            {
                _participant->delete_publisher(_publisher);
                _publisher = nullptr;
            }
            if (_topic)
            {
                _participant->delete_topic(_topic);
                _topic = nullptr;
            }

            _participant = nullptr;
        }

        _subscriberCount = 0;
    }

    void on_publication_matched(DataWriter* writer, const PublicationMatchedStatus &info) override
    {
        _subscriberCount = info.current_count;
        if (_onPublicationMatched)
            _onPublicationMatched(info);
    }


    CALLBACK_DEFINITIONS_TEMPLATE(PublicationMatched, void, const PublicationMatchedStatus& info)
};

#endif //DDSPUBLISHER_H
