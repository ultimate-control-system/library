//
// Created by Teodor on 23.10.2022.
//

#ifndef ODRIVEDDS_DDSUTILITIES_H
#define ODRIVEDDS_DDSUTILITIES_H

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastdds/dds/subscriber/qos/DataReaderQos.hpp>

using namespace eprosima::fastdds::dds;

class ScopedDomainParticipant
{
public:
    explicit ScopedDomainParticipant(const char* name) : _participant(nullptr)
    {
        DomainParticipantQos domainParticipantQos;
        domainParticipantQos.name(name);
        _participant = DomainParticipantFactory::get_instance()->create_participant(0, domainParticipantQos);
    }
    explicit ScopedDomainParticipant(const std::string& name) : _participant(nullptr)
    {
        DomainParticipantQos domainParticipantQos;
        domainParticipantQos.name(name);
        _participant = DomainParticipantFactory::get_instance()->create_participant(0, domainParticipantQos);
    }
    ~ScopedDomainParticipant() { delete _participant; }

    operator bool() const { return _participant; }

    [[nodiscard]] DomainParticipant* participant() { return _participant; }

private:
    DomainParticipant* _participant;
};

#define CALLBACK_DEFINITIONS_TEMPLATE(name, returns, ...)                                           \
public:                                                                                             \
    using name##Signature = returns(__VA_ARGS__);                                                   \
    template <class Type>                                                                           \
    void bindOn##name(Type* instance, returns (Type::*method)(__VA_ARGS__))                         \
    {                                                                                               \
        _on##name = [=](auto&& ...args)                                                             \
        { (instance->*method)(std::forward<decltype(args)>(args)...); };                            \
    }                                                                                               \
    void bindOn##name(std::function<name##Signature> function)                                      \
    { _on##name = std::move(function); }                                                            \
private:                                                                                            \
    std::function<name##Signature> _on##name;


#endif //ODRIVEDDS_DDSUTILITIES_H
