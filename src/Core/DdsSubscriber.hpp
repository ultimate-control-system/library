//
// Created by Teodor on 16.09.2022.
//

#ifndef MY_DDSSUBSCRIBER_H
#define MY_DDSSUBSCRIBER_H

#include <fastdds/dds/subscriber/Subscriber.hpp>
#include <fastdds/dds/subscriber/DataReader.hpp>
#include <fastdds/dds/subscriber/DataReaderListener.hpp>
#include <fastdds/dds/subscriber/SampleInfo.hpp>

#include "DdsUtilities.h"


template<class T>
class DdsSubscriber : public DataReaderListener {
public:
    using MessageType = T;

CALLBACK_DEFINITIONS_TEMPLATE(DataAvailable, void, const typename MessageType::type&, const SampleInfo&)
CALLBACK_DEFINITIONS_TEMPLATE(SubscriptionMatched, void, const SubscriptionMatchedStatus&)

public:
    // ToDo: remember to remove this!
    void testTriggerOnDataAvailable(const typename MessageType::type& msg)
    {
        SampleInfo info;
        _onDataAvailable(msg, info);
    }

    /*!
     * Construct an uninitialized FastDDS subscriber
     */
    DdsSubscriber()
            : _participant(nullptr)
            , _subscriber(nullptr)
            , _reader(nullptr)
            , _topic(nullptr)
            , _type(new MessageType)
            , _connected(false)
    {}

    /*!
     * Construct and initialize a Fast DDS subscriber
     * @tparam S Any string type recognized by eProsima Fast DDS
     * @param participant
     * @param topicName
     */
    template <class S>
    DdsSubscriber(DomainParticipant *participant, S&& topicName)
        : _participant(nullptr)
        , _subscriber(nullptr)
        , _reader(nullptr)
        , _topic(nullptr)
        , _type(new MessageType)
        , _connected(false)
    {
        init(participant, std::forward<S>(topicName));
    }

    ~DdsSubscriber() override
    {
        _destroy();
    }

    /*!
     * Initialize a Fast DDS subscriber
     * @tparam S Any string type recognized by eProsima Fast DDS
     * @param participant
     * @param topicName
     * @return Initialization success
     */
    template <class S>
    bool init(DomainParticipant *participant, S&& topicName)
    {
        _destroy();

        _participant = participant;

        _type.register_type(_participant);

        _topic = _participant->create_topic(topicName, _type->getName(), TOPIC_QOS_DEFAULT);
        if (!_topic)
            return false;

        // Create the Subscriber
        _subscriber = _participant->create_subscriber(SUBSCRIBER_QOS_DEFAULT, nullptr);
        if (!_subscriber)
            return false;

        // Create the DataReader
        _reader = _subscriber->create_datareader(_topic, DATAREADER_QOS_DEFAULT, this);
        if (_reader)
            return false;

        return true;
    }

    [[nodiscard]] bool connected() const { return _connected; }

private:
    DomainParticipant *_participant;
    Subscriber* _subscriber;
    DataReader* _reader;
    Topic* _topic;
    TypeSupport _type;
    bool _connected;

    void _destroy()
    {
        if (_reader && _subscriber)
        {
            _subscriber->delete_datareader(_reader);
            _reader = nullptr;
        }

        if (_participant)
        {
            if (_topic)
            {
                _participant->delete_topic(_topic);
                _topic = nullptr;
            }
            if (_subscriber)
            {
                _participant->delete_subscriber(_subscriber);
                _subscriber = nullptr;
            }

            _participant = nullptr;
        }

        _connected = false;
    }

    void on_subscription_matched(DataReader *, const SubscriptionMatchedStatus &info) override {
        if (info.current_count > 0)
            _connected = true;
        else if (info.current_count <= 0)
            _connected = false;

        if (_onSubscriptionMatched)
            _onSubscriptionMatched(info);
    }

    void on_data_available(DataReader *reader) override
    {
        SampleInfo info;
        typename MessageType::type msg;

        if (reader->take_next_sample(&msg, &info) == ReturnCode_t::RETCODE_OK)
        {
            if (_onDataAvailable)
            {
                _onDataAvailable(msg, info);
            }
        }
    }
};


#endif // MY_DDSSUBSCRIBER_H
